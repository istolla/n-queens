package objects;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Istolla on 6/17/2017.
 */
public class Result {
    private int numberOfQueens;
    private int numberOfGenerations = 1;
    private double averageFitness;
    private double maxIndividualFitness;
    private String fittestSequence;
    private int fittestCollisions;
    private Board fittestBoard;

    public Result(List<Board> boards, int numberOfQueens) {
        this.numberOfQueens = numberOfQueens;
        double populationTotalFitness = boards.stream().mapToDouble(Board::getRawFitness).sum();
        fittestBoard = boards.stream().max(Comparator.comparingDouble(Board::getNormalizedFitness))
                             .get();
        setFittestBoard(
                boards.stream().max(Comparator.comparingDouble(Board::getNormalizedFitness)).get());
        setAverageFitness((populationTotalFitness) / boards.size());
        setMaxIndividualFitness(fittestBoard.getRawFitness());
        setFittestSequence(fittestBoard.toString());
        setFittestCollisions(fittestBoard.getCollisions());
    }

    public Result(List<Board> boards, int numberOfQueens, int numberOfGenerations) {
        this(boards, numberOfQueens);
        setNumberOfGenerations(numberOfGenerations);
    }

    public Board getFittestBoard() {
        return fittestBoard;
    }

    public int getNumberOfGenerations() {
        return numberOfGenerations;
    }

    public void setNumberOfGenerations(int numberOfGenerations) {
        this.numberOfGenerations = numberOfGenerations;
    }

    public double getAverageFitness() {
        return averageFitness;
    }

    public double getMaxIndividualFitness() {
        return maxIndividualFitness;
    }

    public String getFittestSequence() {
        return fittestSequence;
    }

    public double getFittestCollisions() {
        return fittestCollisions;
    }

    private void setFittestBoard(Board fittestBoard) {
        this.fittestBoard = fittestBoard;
    }

    private void setAverageFitness(double averageFitness) {
        this.averageFitness = averageFitness;
    }

    private void setMaxIndividualFitness(double maxIndividualFitness) {
        this.maxIndividualFitness = maxIndividualFitness;
    }

    private void setFittestSequence(String fittestSequence) {
        this.fittestSequence = fittestSequence;
    }

    private void setFittestCollisions(int fittestCollisions) {
        this.fittestCollisions = fittestCollisions;
    }

    @Override
    public String toString() {
        String result = new String();
        String r1 = "Number of queens: " + numberOfQueens;
        String r2 = "Number of generations: " + getNumberOfGenerations();
        String r3 = "Average fitness: " + Double.toString(averageFitness);
        String r4 = "Max individual fitness: " + Double.toString(maxIndividualFitness);
        String r5 = "Fittest sequence: " + fittestSequence;
        String r6 = "Number of collisions for fittest board: " + fittestCollisions;
        result += r1;
        result += "\n";
        result += r2;
        result += "\n";
        result += r3;
        result += "\n";
        result += r4;
        result += "\n";
        result += r5;
        result += "\n";
        result += r6;
        result += "\n";
        return result;
    }
}

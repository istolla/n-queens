package objects;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Istolla on 6/8/2017.
 */
public class Board {
    private int maxCollisions; //The maximum number of collisions.
    private int numberOfQueens; //The numbers of queens
    private int numberOfBits = 0; //The max number of bits needed to represent position on board.
    private double rawFitness; //The raw fitness of this board.
    private double normalizedFitness; //The value of the fitness normalized.
    private int collisions = 0; //The number of collisions on the board.
    private List<Queen> queens = new ArrayList<>(); //List of queens on the board
    private Random random;
    private String genome = new String(); //String representation of genetic material.
    private BitSet chromosome = new BitSet(); //Genetic representation of board solution.

    /**
     * @param numberOfQueens The number of queens on the board.
     */
    public Board(int numberOfQueens) {
        this.numberOfQueens = numberOfQueens;
        initNumberOfBits();
        initMaxCollisions();
        random = new Random(Calendar.getInstance().getTimeInMillis());
        addQueens(); //Add the queens to the board.
        initChromosome(); //Set the genetic code representing board solution
    }

    /**
     * @param chromosome     The genetic sequence that determines the position of queens on this
     *                       board. Sections of the chromosome determine the position of the
     *                       queen in a column. Sections are sequential so each section
     *                       correspond to a column.
     * @param numberOfQueens The number of queens on the board.
     */
    public Board(BitSet chromosome, int numberOfQueens) {
        this.chromosome = chromosome;
        this.numberOfQueens = numberOfQueens;
        random = new Random(Calendar.getInstance().getTimeInMillis());
        initNumberOfBits();
        initMaxCollisions();
        addQueens(chromosome);
    }

    /**
     * @param chromosome String representation of the genetic sequence that determines the position
     *                   of queens on this board. Sections of the chromosome determine the
     *                   position of the queen in a column. Sections are sequential so each section
     *                   correspond to a column.
     */
    public Board(String chromosome, int numberOfQueens) {
        for (int i = 0; i < chromosome.length(); i++)
            if (chromosome.charAt(i) == '1')
                this.chromosome.set(i);
        this.numberOfQueens = numberOfQueens;
        initNumberOfBits();
        initMaxCollisions();
        addQueens(this.chromosome);
    }

    /**
     * Returns the raw fitness.
     *
     * @return The value of the raw fitness of this board.
     */
    public double getRawFitness() {
        return rawFitness;
    }

    /**
     * Returns the normalized fitness.
     *
     * @return The value of the fitness normalized.
     */
    public double getNormalizedFitness() {
        return normalizedFitness;
    }

    /**
     * Sets the value of the normalized fitness.
     *
     * @param normalizedFitness The value of the normalized fitness.
     */
    public void setNormalizedFitness(double normalizedFitness) {
        this.normalizedFitness = normalizedFitness;
    }

    /**
     * Returns the genetic material that determines queen placement on this board.
     *
     * @return The genetic sequence for the position of queens on this board.
     */
    public BitSet getChromosome() {
        return chromosome;
    }

    /**
     * Returns the number of collisions occurring on this board.
     *
     * @return The number of queens attacking each other on the board.
     */
    public int getCollisions() {
        return collisions;
    }

    @Override
    public String toString() {
        return genome;
    }

    /**
     * Calculates the minimum number of bits required to represent the maximum position in a
     * column on this board.
     *
     * @return The number of bits required to represent the largest position in a column of the
     * board.
     */
    private void initNumberOfBits() {
        while (numberOfQueens > Math.pow(2, numberOfBits))
            numberOfBits++;
    }

    /**
     * Adds queens to the board at random position in each column.
     */
    private void addQueens() {
        for (int i = 0; i < numberOfQueens; i++) {
            int x = random.nextInt(numberOfQueens);
            queens.add(new Queen(getPositionBinary(x)));
            if (queens.size() > 1)
                updateCollisions();
        }
        updateRawFitness();
    }

    /**
     * Adds queens to the board at random position in each column.
     *
     * @param chromosome The genetic sequence that determines the position of queens on this
     *                   board.
     */
    private void addQueens(BitSet chromosome) {
        String gene = new String();
        int end = numberOfBits * numberOfQueens;
        for (int i = 0; i < end; i++) {
            if (chromosome.get(i))
                gene += '1';
            else
                gene += '0';
            if (gene.length() == numberOfBits) {
                /*If the gene to be added expresses a position beyond the board, change that gene
                 to represent the maximum column position.*/
                if (Integer.parseInt(gene, 2) > numberOfQueens - 1)
                    gene = Integer.toBinaryString(numberOfQueens - 1);
                queens.add(new Queen(gene));
                genome += gene;
                gene = "";
                if (queens.size() > 1)
                    updateCollisions();
            }
        }
        updateRawFitness();
    }

    /**
     * Updates the raw fitness based on the number of collisions on the board. Note that a
     * collision is the number of queen attacks on the board.
     */
    private void updateRawFitness() {
        rawFitness = (double) (maxCollisions - collisions) / maxCollisions;
    }

    /**
     * Returns a binary string representation of a position on a column of the board.
     */
    private String getPositionBinary(int positionIndex) {
        String result = Integer.toBinaryString(positionIndex);
        while (result.length() < numberOfBits)
            result = '0' + result;
        return result;
    }

    /**
     * To be used after each queen is added to a column. Updates the collision variable with the
     * number of attacking queens on the board.
     */
    private void updateCollisions() {
        int lastQueenIndex = queens.size() - 1;
        for (int i = lastQueenIndex - 1, j = 1; i >= 0; i--, j++) {
            if (queens.get(lastQueenIndex).getPosition() - j == queens.get(i).getPosition())
                collisions++;
            if (queens.get(lastQueenIndex).getPosition() + j == queens.get(i).getPosition())
                collisions++;
            if (queens.get(lastQueenIndex).getPosition() == queens.get(i).getPosition())
                collisions++;
        }
    }

    /**
     * Determines the maximum number of collisions a board can have based on the number of queens
     * on it.
     */
    private void initMaxCollisions() {
        maxCollisions = (numberOfQueens * (numberOfQueens - 1)) / 2;
    }

    /**
     * Sets up the genetic code representing the board solution.
     */
    private void initChromosome() {
        genome = queens.stream().map(Queen::getGene).collect(Collectors.joining());
        for (int i = 0; i < (numberOfQueens * numberOfBits); i++)
            if (genome.charAt(i) == '1')
                chromosome.set(i);
    }
}

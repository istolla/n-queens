package objects;

import java.security.InvalidParameterException;

/**
 * Created by Istolla on 6/17/2017.
 */
class Queen {
    private int position;
    private String gene;

    /**
     * @param gene A string binary representation of the position of this queen in a column of a
     *             board.
     */
    public Queen(String gene) {
        setGene(gene);
    }

    /**Returns a string representation of the position of this queen in a column of a board.
     * @return Returns the string representation of the position of this queen in a column of a
     * board.
     */
    public String getGene() {
        return gene;
    }

    /**
     * @param gene The string binary representation of the position of this queen in a column of a
     *             board.
     */
    public void setGene(String gene) {
        if (!gene.matches("[01]+"))
            throw new InvalidParameterException("Parameter must only contain \'0\' or \'1\'");
        this.gene = gene;
        String temp = gene;
        position = Integer.parseInt(gene, 2);
    }

    /**
     * @return The position index of this queen on a column of a board.
     */
    public int getPosition() {
        return position;
    }
}

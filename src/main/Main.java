package main;

import evolution.Genetics;

/**
 * Created by Istolla on 6/17/2017.
 */
public class Main {
    public static void main(String... args) {
        Genetics population = new Genetics(10);
        population.simulateEvolution();
        System.out.println(population.getResult());
    }
}

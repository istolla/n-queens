package evolution;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Comparator;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Random;

import objects.Board;
import objects.Result;

/**
 * Created by Istolla on 6/17/2017.
 */
public class Genetics {
    private static final int POPULATION_SIZE = 175; //The population size for each generation
    private static final int GENERATION_LIMIT = 100000;// The maximum number of generations
    private static final double CROSSOVER_PROBABILITY = 0.8; //Probability of crossover
    private static final double MUTATION_PROBABILITY = 0.01; //Probability of mutation occurrence
    private static final double NORMALIZED_MINIMUM = .05; //5% Chance of selection
    private static final double NORMALIZED_MAXIMUM = .20; //20% Chance of selection
    private int numberOfQueens; //The number of queens for every board.
    private int sequenceLength;
    private int generation = 0;
    private Random random;
    private List<Board> boards;
    private int lastParentIndex = -1;
    private Result result;

    /**
     * @param numberOfQueens The number of queens in the problem to be solved.
     */
    public Genetics(int numberOfQueens) {
        this.numberOfQueens = numberOfQueens;
        boards = new ArrayList<>();
        for (int i = 0; i < POPULATION_SIZE; i++)
            boards.add(new Board(numberOfQueens));
        initSequenceLength();
        random = new Random(Calendar.getInstance().getTimeInMillis());
        normalizeFitness();
    }

    /**
     * @param boards         The initial boards and configurations.
     * @param numberOfQueens The number of queens in the problem to be solved.
     */
    public Genetics(List<Board> boards, int numberOfQueens) {
        this.boards = boards;
        this.numberOfQueens = numberOfQueens;
        initSequenceLength();
        random = new Random(Calendar.getInstance().getTimeInMillis());
        normalizeFitness(); //Normalize the fitness of the list of board solutions.
    }

    /**
     * Simulate evolution for a given number of generations.
     *
     * @param numberOfGenerations The number of generations for the simulations to generate.
     */
    public void simulateEvolution(int numberOfGenerations) {
        for (int i = 0; i < numberOfGenerations; i++)
            nextGeneration();
        result = new Result(boards, numberOfQueens, generation);
    }

    /**
     * Simulate evolution until a solution is found or the generation limit is reached.
     */
    public void simulateEvolution() {
        result = new Result(boards, numberOfQueens);
        while (result.getFittestBoard().getRawFitness() != 1.0 && generation < GENERATION_LIMIT) {
            nextGeneration();
            result = new Result(boards, numberOfQueens, generation);
        }
    }

    /**
     * Returns the result of evolution simulation.
     */
    public Result getResult() {
        return result;
    }

    private void initSequenceLength() {
        sequenceLength = 0;
        while (numberOfQueens > Math.pow(2, sequenceLength))
            sequenceLength++;
        sequenceLength *= numberOfQueens;
    }

    /**
     * Simulate the next generation.
     */
    private void nextGeneration() {
        List<Board> newGeneration = new ArrayList<>();
        while (newGeneration.size() < POPULATION_SIZE) {
            Board p1 = boards.get(getParentIndex()), p2 = boards.get(getParentIndex());
            Board[] offspring = breed(p1, p2);
            if (newGeneration.size() + 1 == POPULATION_SIZE)
                newGeneration.add(offspring[random.nextInt(2)]);
            else
                newGeneration.addAll(Arrays.asList(offspring));
        }
        boards.clear();
        boards.addAll(newGeneration);
        normalizeFitness();
        generation++;
    }

    /**
     * Mate two parents.
     *
     * @return Array of offspring.
     */
    private Board[] breed(Board p1, Board p2) {
        Board[] offspring = new Board[2]; //Used to store resulting children.
        BitSet dna1 = p1.getChromosome(), dna2 = p2.getChromosome();
        if (random.nextDouble() < CROSSOVER_PROBABILITY) {
            int crossoverPoint = random.nextInt(sequenceLength);
            BitSet c1 = new BitSet(), c2 = new BitSet(), c3 = new BitSet(), c4 = new BitSet();
            c1.or(dna1);
            c1.clear(0, crossoverPoint);
            c2.or(dna1);
            c2.clear(crossoverPoint, sequenceLength);
            c3.or(dna2);
            c3.clear(0, crossoverPoint);
            c4.or(dna2);
            c4.clear(crossoverPoint, sequenceLength);

                /*Recombine*/
            dna1 = new BitSet();
            dna2 = new BitSet();
            dna1.or(c1);
            dna1.or(c4);
            dna2.or(c2);
            dna2.or(c3);

            /*Mutate*/
            mutate(dna1);
            mutate(dna2);

            /*Create Offspring*/
            offspring[0] = new Board(dna1, numberOfQueens);
            offspring[1] = new Board(dna2, numberOfQueens);
        } else {
            offspring[0] = new Board(dna1, numberOfQueens);
            offspring[1] = new Board(dna2, numberOfQueens);
        }
        return offspring;
    }

    /**
     * Simulate mutation of a sequence of dna.
     */
    private void mutate(BitSet dna) {
        for (int i = 0; i < sequenceLength; i++)
            if (random.nextDouble() < MUTATION_PROBABILITY)
                dna.flip(i);
    }

    /**
     * Normalize the fitness of each board.
     */
    private void normalizeFitness() {
        DoubleSummaryStatistics statistics = boards.stream().mapToDouble(Board::getRawFitness)
                                                   .summaryStatistics();
        double max = statistics.getMax(), min = statistics.getMin();

        /*Normalize the fitness for each board. Note that if max == min all boards have the same
        fitness.*/
        if (min != max) {
            boards.forEach(board -> board.setNormalizedFitness(NORMALIZED_MINIMUM + (
                    ((board.getRawFitness() - min) * (NORMALIZED_MAXIMUM - NORMALIZED_MINIMUM)) / (
                            max - min))));
        } else
            boards.forEach(board -> board.setNormalizedFitness(NORMALIZED_MAXIMUM));
    }

    /**
     * Get the index of parent selected for mating.
     *
     * @return The index of the selected parent.
     */
    private int getParentIndex() {
        int parentIndex = 0;
        double totalFitness = boards.stream().mapToDouble(Board::getNormalizedFitness).sum();
        double target = totalFitness * random.nextDouble();
        boards.sort(Comparator.comparingDouble(Board::getRawFitness)
                              .reversed()); //Sort boards by fitness.
        double sum = 0;
        while (sum < target) {
            if (parentIndex != lastParentIndex)
                sum += boards.get(parentIndex).getNormalizedFitness();
            parentIndex++;
            parentIndex %= POPULATION_SIZE;
        }
        lastParentIndex = parentIndex;
        return parentIndex;
    }
}